# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

insurances = Insurance.create ([{ name: 'Aetna' , street_address: '4256 El Paso'},
                                { name: 'AIG' , street_address: '4636 Houston Rd'},
                                { name: 'Blue Cross' , street_address: '5245 Minnesota'},
                                { name: 'Texans Red' , street_address: '8555 Travis St'},
                                { name: 'United Health' , street_address: '8965 Smith'}])

specialists = Specialist.create([{ name: 'Dr.Hardings', specialty:'Cardiologist' },
                                 { name: 'Dr.Huessein', specialty:'Dermatologist' },
                                 { name: 'Dr.Kent', specialty:'Family Physician' },
                                 { name: 'Dr.Ortega', specialty:'Gyneocologist' },
                                 { name: 'Dr.Smitch', specialty:'Neurologist' }])

patients = Patient.create([{ name: 'Erica Rodriguez', street_address: '4893 Cypress', :insurance_id => 2 },
                           { name: 'Joe Gonzalez', street_address: '5343 West Loop', :insurance_id => 1 },
                           { name: 'Martha Floyd', street_address: '7894 Grovewood', :insurance_id => 3 },
                           { name: 'Robin Dye', street_address: '9044 Clay Rd', :insurance_id => 4 },
                           { name: 'Victor Flores', street_address: '9434 Wynwood', :insurance_id => 5 }])

appointments =(
 Appointment.create( :specialist_id => 1, :patient_id => 1, complaint: 'Irregular heartbeat', appointment_date: '2014-10-30 ', fee: '120.34')
 Appointment.create( :specialist_id => 3, :patient_id => 1, complaint: 'Breathing heavily', appointment_date: '2014-09-30 ', fee: '50')
 Appointment.create( :specialist_id => 5, :patient_id => 2, complaint: 'Throbbing headache ', appointment_date: '2014-10-12 ', fee: '150')
 Appointment.create( :specialist_id => 2, :patient_id => 2, complaint: 'Itching, burning on right arm', appointment_date: '2014-11-16 ', fee: '115.75')
 Appointment.create( :specialist_id => 4, :patient_id => 3, complaint: 'Woman check-up', appointment_date: '2014-10-15 ', fee: '275.05')
 Appointment.create( :specialist_id => 3, :patient_id => 3, complaint: 'Physical check-up', appointment_date: '2014-11-18 ', fee: '125.03')
 Appointment.create( :specialist_id => 5, :patient_id => 4, complaint: 'Suffering migraines', appointment_date: '2014-11-25 ', fee: '150.88')
 Appointment.create( :specialist_id => 2, :patient_id => 4, complaint: 'Testing for skin allergies', appointment_date: '2014-12-01 ', fee: '267.68')
 Appointment.create( :specialist_id => 1, :patient_id => 5, complaint: 'Heart murmur, needs electrocardiogram ', appointment_date: '2014-11-30 ', fee: '350')
 Appointment.create( :specialist_id => 3, :patient_id => 5, complaint: 'Vomiting and fever', appointment_date: '2014-12-13 ', fee: '150.45'))